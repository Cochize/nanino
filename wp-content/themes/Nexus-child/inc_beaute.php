				<?php for ($i=1; $i <= 1; $i++) { ?>
					<?php query_posts('category_name=beaute'); while (have_posts()) : the_post(); ?>
						<?php 
							global $more; 
							$more = 0;
						?>

						<div class="service">
							<?php 
									$thumb = '';
									$classtext = 'item-image';
									$width = (int) apply_filters( 'et_index_image_width', 360 );
									$height = (int) apply_filters( 'et_index_image_height', 152 );
									$titletext = get_the_title();
									$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
									$thumb = $thumbnail["thumb"];
									$et_service_link = get_post_meta($post->ID,'etlink',true) ? get_post_meta($post->ID,'etlink',true) : get_permalink();
							?>
							<div class="logo-marque"><?php the_post_thumbnail(); ?></div>
							<h2 class="title"><a href="<?php echo $et_service_link; ?>"><?php the_title(); ?></a></h2>


							<?php if ( ! has_excerpt() ) {

								$nbr = strlen($titletext); 
								if ($nbr > 40){
									?>
									<p><?php truncate_post(110); ?></p>
								<?php
									} 
								else{
								?>
								<p><?php truncate_post(140); ?></p>
								<?php
									} 

							} else { 
							      the_excerpt();
							}
							?>

							<div class="thumb2">
								<a href="<?php echo $et_service_link; ?>">
									<?php if (class_exists('MultiPostThumbnails')) :
										MultiPostThumbnails::the_post_thumbnail(
										get_post_type(),
										'secondary-image'
									);
									endif; ?>
								</a>
							</div>
							<span class="link-service"><a href="<?php echo $et_service_link; ?>">Les produits</a></span>
						</div> <!-- end .service -->

					<?php endwhile; wp_reset_query(); ?>

				<?php } ?>
				<div class="clear"></div>