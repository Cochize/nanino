<?php
/*
Template Name: Actualites
*/
?>
<?php get_header(); ?>

<?php
$featured_image = false;

if ( '' != get_the_post_thumbnail() ) :
	$featured_image = true;
?>
<div class="post-thumbnail">
	<div class="container">
		<h1 class="post-heading"><?php the_title(); ?></h1>
	</div> <!-- .container -->
</div> <!-- .post-thumbnail -->
<?php endif; ?>

<div class="page-wrap container fullwidth">
	<div id="main-content-actu">
		<div class="main-content-wrap clearfix">
			<div id="content">
			<section id="actu-module-home">
				<div class="actu-tile"><p id="fb">Encore plus d&rsquo;actualit&eacute;s sur: <a target="_blank" href="http://www.facebook.com/catliegeoise">facebook</a></p>
					<h1>Anciennes actualités</h1></div>
				<?php for ($i=1; $i <= 1; $i++) { ?>
					<?php query_posts('category_name=actualites&offset=6'); while (have_posts()) : the_post(); ?>
						<?php 
							global $more; $more = 0;
						?>
						<div class="service_archive">
							<?php 
									$thumb = '';
									$classtext = 'item-image';
									$width = (int) apply_filters( 'et_index_image_width', 578 );
									$height = (int) apply_filters( 'et_index_image_height', 208 );
									$titletext = get_the_title();
									$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
									$thumb = $thumbnail["thumb"];
									$et_service_link = get_post_meta($post->ID,'etlink',true) ? get_post_meta($post->ID,'etlink',true) : get_permalink();
									$category = get_the_category(); 
									$the_category_id = $category[0]->cat_ID;
							?>
							<h2 class="title"><a href="<?php echo $et_service_link; ?>"><?php the_title(); ?></a></h2>
							<?php if ( $thumb <> '' ) {  
								?>
								<div class="thumb2">

									<a href="<?php echo $et_service_link; ?>" style="border-top: 3px solid <?php echo rl_color($the_category_id); ?>;">
										<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
									</a>
								</div>
							<?php 
								} ?>
							<div class="meta">Posté le <?php the_date('d-m-Y', '<span>', '</span>'); ?> </div>
							<?php 
								$nbr = strlen($titletext); 
								if ($nbr > 90){
							?>
								<p><?php truncate_post(90); ?></p>
							<?php
								} 
								else{
							?>
								<p><?php truncate_post(140); ?></p>
							<?php
								} 
							?>
							
							<span class="link-service">&rarr; <a href="<?php echo $et_service_link; ?>">Lire la suite</a></span>
						</div> <!-- end .service -->

					<?php endwhile; wp_reset_query(); ?>

				<?php } ?>
				<div class="clear"></div>

			</section>

			</div> <!-- #content -->
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

<?php get_footer(); ?>