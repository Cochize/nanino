<?php
/*
Template Name: Gamme Propalia 4
*/
?>
<?php get_header(); ?>

<?php
$featured_image = true;

?>

<div class="page-wrap fullwidth ombre">
	<div class="container">
				<div id="left-area">

					<div id="half3">
						<?php while ( have_posts() ) : the_post(); ?>
						<article class="entry-content clearfix">
						<h1 class="main-title"><?php the_title(); ?></h1>
						<?php
							the_content();
							wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Nexus' ), 'after' => '</div>' ) );
						?>
						</article> <!-- .entry -->
						<?php if (et_get_option('nexus_integration_single_bottom') <> '' && et_get_option('nexus_integrate_singlebottom_enable') == 'on') echo(et_get_option('nexus_integration_single_bottom')); ?>
						<?php endwhile; ?>
					</div>
					<div id="clr"></div>
				</div>
	</div>
</div>	
<!--catégories-->
<div class="container2">	
<div class="main-content-wrap clearfix">
			<div id="content">
					<div class="categories-title">
						<h3><span class="green">Les purifiants</span></h3>
					</div>
					<div class="categories2">
				<?php for ($i=1; $i <= 1; $i++) { ?>
					<?php query_posts('category_name=propolia-g10'); while (have_posts()) : the_post(); ?>
						<?php 
							global $more; 
							$more = 0;
							if ($i++ % 2 == 0)
						    {
						 ?>
							 <div class="categorie2" id="pair">
							 	<div class="content_categorie2">
									<h3 class="title"><?php the_title(); ?></h3>
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						    else
						    {
						 ?>
							 <div class="categorie2" id="impair">
							 	<div class="content_categorie2">
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						?>
						


					<?php endwhile; wp_reset_query(); ?>
				<?php } ?>
				<div id="clr"></div>
				</div>

			</div> <!-- #content -->
			<?php get_sidebar(); ?>
		</div> <!-- .main-content-wrap -->
	</div>
	<!--End-catégories-->
	<!--catégories-->
<div class="container2">	
<div class="main-content-wrap clearfix">
			<div id="content">
					<div class="categories-title">
						<h3><span class="green">Les purifiants</span></h3>
					</div>
					<div class="categories2">
				<?php for ($i=1; $i <= 1; $i++) { ?>
					<?php query_posts('category_name=propolia-g11'); while (have_posts()) : the_post(); ?>
						<?php 
							global $more; 
							$more = 0;
							if ($i++ % 2 == 0)
						    {
						 ?>
							 <div class="categorie2" id="pair">
							 	<div class="content_categorie2">
									<h3 class="title"><?php the_title(); ?></h3>
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						    else
						    {
						 ?>
							 <div class="categorie2" id="impair">
							 	<div class="content_categorie2">
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						?>
						


					<?php endwhile; wp_reset_query(); ?>
				<?php } ?>
				<div id="clr"></div>
				</div>

			</div> <!-- #content -->
			<?php get_sidebar(); ?>
		</div> <!-- .main-content-wrap -->
	</div>
	<!--End-catégories-->
		<!--catégories-->
<div class="container2">	
<div class="main-content-wrap clearfix">
			<div id="content">
					<div class="categories-title">
						<h3><span class="green">Les purifiants</span></h3>
					</div>
					<div class="categories2">
				<?php for ($i=1; $i <= 1; $i++) { ?>
					<?php query_posts('category_name=propolia-g12'); while (have_posts()) : the_post(); ?>
						<?php 
							global $more; 
							$more = 0;
							if ($i++ % 2 == 0)
						    {
						 ?>
							 <div class="categorie2" id="pair">
							 	<div class="content_categorie2">
									<h3 class="title"><?php the_title(); ?></h3>
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						    else
						    {
						 ?>
							 <div class="categorie2" id="impair">
							 	<div class="content_categorie2">
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						?>
						


					<?php endwhile; wp_reset_query(); ?>
				<?php } ?>
				<div id="clr"></div>
				</div>

			</div> <!-- #content -->
			<?php get_sidebar(); ?>
		</div> <!-- .main-content-wrap -->
	</div>
	<!--End-catégories-->
		
<div class="page-wrap container fullwidth">
	<div id="main-content">
		<div class="main-content-wrap clearfix">
			<div id="content">

				<div id="left-area">
					


				</div> 	<!-- end #left-area -->
			</div> <!-- #content -->

			<?php //get_sidebar(); ?>
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

	<?php get_footer(); ?>