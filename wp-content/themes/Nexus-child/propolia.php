<?php
/*
Template Name: Propolia
*/
?>
<?php get_header(); ?>

<?php
$featured_image = true;

?>
<div class="post-thumbnail">
	<div class="container">
		
	</div> 
</div> 

<div class="page-wrap fullwidth ombre">
	<div class="container">
				<div id="left-area">
					<div id="half1">
									<?php if (class_exists('MultiPostThumbnails')) :
										MultiPostThumbnails::the_post_thumbnail(
										get_post_type(),
										'secondary-image'
									);
									endif; ?>
					</div>	
					<div id="half2">
						<?php while ( have_posts() ) : the_post(); ?>
						<article class="entry-content clearfix">
						<h1 class="main-title"><?php the_title(); ?></h1>
						<?php
							the_content();
							wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Nexus' ), 'after' => '</div>' ) );
						?>
						</article> <!-- .entry -->
						<?php if (et_get_option('nexus_integration_single_bottom') <> '' && et_get_option('nexus_integrate_singlebottom_enable') == 'on') echo(et_get_option('nexus_integration_single_bottom')); ?>
						<?php endwhile; ?>
					</div>
					<div id="clr"></div>
				</div>
	</div>
</div>				


					<div class="categories-title">
						<h3>Type de <span class="green">produits</span></h3>
					</div>
					<div class="categories">
				<?php for ($i=1; $i <= 1; $i++) { ?>
					<?php query_posts('category_name=propolia'); while (have_posts()) : the_post(); ?>
						<?php 
							global $more; 
							$more = 0;
							if ($i++ % 2 == 0)
						    {
						 ?>
							 <div class="categorie" id="pair">
							 	<div class="content_categorie">
									<h3 class="title"><?php the_title(); ?></h3>
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						    else
						    {
						 ?>
							 <div class="categorie" id="impair">
							 	<div class="content_categorie">
									<h3 class="title"><?php the_title(); ?></h3>
									<?php the_content(); ?>
								</div>
							</div> 
						 <?php
						    }
						?>
						


					<?php endwhile; wp_reset_query(); ?>
				<?php } ?>
				<div id="clr"></div>
				</div>

<div class="page-wrap container fullwidth">
	<div id="main-content">
		<div class="main-content-wrap clearfix">
			<div id="content">

				<div id="left-area">
					


				</div> 	<!-- end #left-area -->
			</div> <!-- #content -->

			<?php //get_sidebar(); ?>
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

	<?php get_footer(); ?>